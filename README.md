# Bracketer

Builds brackets given a list of the names for the bracket.

## Usage:
```
$ bracketer.exe <filename> [options]
```

### Avaliable options
| Option | Description|
|--------|------------|
|<code>--randomize, -r</code>| Randomize the names after loading in.|

### Example output:
```
$ bracketer.exe names.txt

-----------
           |-John Doe--
-----------            |
                       |-----------
-----------            |           |
           |-Joe Smith-            |
-----------                        |
                                   |-----------
-Jane Doe--                        |
           |-----------            |
-Bobby Joe-            |           |
                       |-----------
-Billy Bob-            |
           |-----------
-----------
```

## Input file format
The input file should be a list of comma-separated names. The names can all be
on one line or on separate lines. Examples:

```
John Doe, Joe Smith, Jane Doe
```

```
John Doe,
Joe Smith,
Jane Doe
```

## Compiling:
```
$ mcs bracket.cs bracketer.cs -out:bracketer.exe
```

## Running tests:
```
$ mcs bracket.cs test_bracketer.cs -out:test.exe
$ test.exe test_bracketer.in test_bracketer.out
```
