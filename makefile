run: build
	mono bracketer.exe n

test: clean
	mcs bracket.cs test_bracketer.cs -out:test.exe
	mono test.exe test_bracketer.in test_bracketer.out

build: clean
	mcs bracket.cs bracketer.cs -out:bracketer.exe

clean:
	-rm -f *.dll
	-rm -f *.exe
