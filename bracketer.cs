namespace Bracketer {

    using System;
    using System.Linq;
    using System.Collections.Generic;
    
    public class Bracketer {

        public static void Main(string[] args) {
            if (args.Length == 0) {
                Console.WriteLine("Incorrect number of arguments. Missing " +
                        "required argument filename.");
                printUsage();
            } else {
                string filename = args[0];
                List<string> argsL = new List<string>(args.Skip(1));
                Bracket b = new Bracket(getNames(filename), randomize(argsL));
                Console.WriteLine(b);
            }
        }

        /**
         * Prints the usage
         */
        private static void printUsage() {
            Console.WriteLine(
                    "Usage:\n" +
                    "\tbracketer.exe <filename> [options]");
        }

        /**
         * Gets whether or not should randomize the names
         * args:    the list of arguments
         * Returns true if args contains "--randomize" or "-r", false otherwise
         */
        private static bool randomize(List<string> args) {
            return args.Contains("--randomize") || args.Contains("-r");
        }

        /**
         * Loads in the names from the given file.
         * filename:    the name of the file
         * Returns the names
         */
        private static string[] getNames(string filename) {
            string[] lines = System.IO.File.ReadAllLines(filename);
            return trimNames(lines);
        }

        /**
         * Splits the names based on "," and trims extra whitespace.
         * names:   the array of names
         * Returns the split and trimmed names
         */
        private static string[] trimNames(string[] names) {
            IEnumerable<string> it = String.Join(" ", names).Split(",")
                    .Select(n => n.Trim());
            List<string> namesL = new List<string>(it);
            while (namesL.Remove("")) {}
            return namesL.ToArray();
        }
    }
}
