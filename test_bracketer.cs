namespace Bracketer {

    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class TestBracketer {
        public static void Main(string[] args) {
            if (args.Length < 2) {
                Console.WriteLine("Missing required arguments infile, outfile");
            } else {
                string[] inlines = getInputLines(args[0]);
                string[] outlines = getOutputLines(args[1]);
                runTests(inlines, outlines);
            }
        }

        /**
         * Gets the input lines to test removing blank lines
         * infile:  the name of the file to read from
         * Returns an array of the input lines
         */
        private static string[] getInputLines(string infile) {
            string[] inlines = System.IO.File.ReadAllLines(infile);
            List<string> result = new List<string>();
            for (int i = 0; i < inlines.Length; i++) {
                if (inlines[i].Length > 0) {
                    result.Add(inlines[i]);
                }
            }

            return result.ToArray();
        }

        /**
         * Gets the output lines combining lines that belong to the same test
         * outfile:     the name of the file to read from
         * Returns an array of the expected output lines
         */
        private static string[] getOutputLines(string outfile) {
            string[] lines = System.IO.File.ReadAllLines(outfile);
            List<string> result = new List<string>();

            string s = "";
            for (int i = 0; i < lines.Length; i++) {
                if (lines[i] == ";") {
                    result.Add(s);
                    s = "";
                } else {
                    s += lines[i] + "\n";
                }
            }

            return result.ToArray();
        }

        /**
         * Runs the tests
         * inlines:     the array of input lines
         * outlines:    the array of expected output lines
         */
        private static void runTests(string[] inlines, string[] outlines) {
            int passed = 0;
            int total = 0;
            List<string> failedInputs = new List<string>();

            for (int i = 0; i < inlines.Length; i++) {
                total++;
                Bracket b = new Bracket(trimNames(inlines[i]));
                if (b.ToString().Equals(outlines[i])) {
                    passed++;
                } else {
                    failedInputs.Add(inlines[i]);
                }
            }

            for (int i = 0; i < failedInputs.Count; i++) {
                Console.WriteLine("FAILED: " + failedInputs[i]);
            }

            Console.WriteLine("Test ran: " + total + ", passed: " + passed +
                    ", failed: " + (total - passed));
        }

        /**
         * Splits the input line of names into an array of names
         * names:   the string of names
         * Returns an array of names
         */
        private static string[] trimNames(string names) {
            IEnumerable<string> it = names.Split(",").Select(n => n.Trim());
            List<string> namesL = new List<string>(it);
            while (namesL.Remove("")) {}
            return namesL.ToArray();
        }
    }
}
