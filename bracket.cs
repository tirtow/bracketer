namespace Bracketer {

    using System;
    using System.Collections.Generic;

    public class Bracket {

        private Node root;
        private int numCompetitors;

        /**
         * Creates a new Bracket with the given names.
         * Byes are assigned starting at the beginning of the array of names
         * names:   the names to use
         */
        public Bracket(string[] names) :this(names, false) {}

        /**
         * Creates a new Bracket with the given names randomizing the names.
         * names:       the names to use
         * randomize:   whether or not to randomize the names
         */
        public Bracket(string[] names, bool randomize) {
            if (randomize) {
                names = shuffleNames(names);
            }

            this.numCompetitors = names.Length;
            this.root = buildTree();
            setNames(names);
        }

        /**
         * Randomizes the order of the given array of names
         * names:   the array to randomize
         * Returns the randomized array
         */
        private string[] shuffleNames(string[] names) {
            List<string> namesL = new List<string>(names);

            Random random = new Random();
            string[] result = new string[names.Length];
            for (int i = 0; i < names.Length; i++) {
                int index = random.Next(0, namesL.Count);
                result[i] = namesL[index];
                namesL.RemoveAt(index);
            }

            return result;
        }

        /**
         * Builds the empty tree of Nodes for this bracket
         * Returns the root node
         */
        private Node buildTree() {
            Node root = new Node();
            buildTree(root, 1);
            return root;
        }

        /**
         * Helper method to build the tree of empty Nodes recursively
         * node:    the parent Node
         * layer:   the current layer
         */
        private void buildTree(Node node, int layer) {
            if (layer <= numLayers()) {
                node.top = new Node();
                node.bottom = new Node();
                buildTree(node.top, layer + 1);
                buildTree(node.bottom, layer + 1);
            }
        }

        /**
         * Sets the names of the competitors in the bracket
         * names:   the array of names
         */
        private void setNames(string[] names) {
            List<string> namesL = new List<string>(names);
            namesL.Reverse();
            setNames(root.top, 1, namesL);
        }

        /**
         * Helper to set the names of the competitors.
         * Traverses through the bracket tree breadth-first setting bye names
         * first then the non-bye names.
         * node:    the current Node
         * layer:   the current layer
         * names:   the List of names in reverse order they are to be inserted
         *          in
         */
        private void setNames(Node node, int layer, List<string> names) {
            if (names.Count > 0) {
                // Set the layer number to check
                int layerCheck = layer;
                if (byesLeft(names.Count) > 0) {
                    layerCheck++;
                }

                if (layerCheck == numLayers()) {
                    // At the correct layer to insert
                    node.name = names[names.Count - 1];
                    names.RemoveAt(names.Count - 1);
                } else {
                    // Not at a deep enough layer, traverse top then bottom
                    // children
                    setNames(node.top, layer + 1, names);
                    setNames(node.bottom, layer + 1, names);
                }
            }
        }

        /**
         * Calculates the number of byes left given the number of names left
         * numNames:    the number of names left
         * Returns the number of byes left
         */
        private int byesLeft(int numNames) {
            return Math.Max(0, numNames - (numCompetitors - byes()));
        }

        /**
         * Calculates the number of byes based on the number of competitors
         */
        public int byes() {
            int exp = (int) Math.Ceiling(Math.Log(numCompetitors) / Math.Log(2));
            return ((int) Math.Pow(2, exp)) - numCompetitors;
        }

        /**
         * Builds the string of the Bracket
         */
        public override string ToString() {
            int charCount = longestName() + 1;
            List<string> lines = new List<string>();
            buildBracketString(root.top, 1, charCount, lines);
            setVerticalConnectors(lines, charCount);

            string result = "";
            for (int i = 0; i < lines.Count; i++) {
                result += lines[i] + "\n";
            }
            return result;
        }

        /**
         * Helper to recursively build the bracket string
         * node:        the current Node
         * layer:       the current layer
         * charCount:   the length of the longest string
         * lines:       the List of strings to add lines to
         */
        private void buildBracketString(Node node, int layer, int charCount,
                List<string> lines) {
            if (node != null) {
                buildBracketString(node.top, layer + 1, charCount, lines);

                string spaces = buildSpacesString(layer, charCount);
                lines.Add(spaces + buildName(node, charCount));

                buildBracketString(node.bottom, layer + 1, charCount, lines);
            }
        }

        /**
         * Builds the string for the name padding extra spaces with "-"
         * node:        the Node to get the name of
         * charCount:   the number of characters the longest name is
         * Returns the name string
         */
        private string buildName(Node node, int charCount) {
            string name = "-" + node.name;
            for (int i = 0; i < charCount - node.name.Length; i++) {
                name += "-";
            }

            return name;
        }

        /**
         * Builds the string of spaces to pad the front of the entry with
         * layer:       the current layer
         * charCount:   the number of characters the longest name is
         * Returns the string of spaces
         */
        private string buildSpacesString(int layer, int charCount) {
            string result = "";
            for (int i = 0; i < (numLayers() - layer) * (charCount + 2); i++) {
                result += " ";
            }

            return result;
        }

        /**
         * Sets the vertical connectors in the string output
         * lines:       the list of lines
         * charCount:   the number of characters used for the name in a line
         */
        private void setVerticalConnectors(List<string> lines, int charCount) {
            // Line #2 will always have a connector at one "indent" in then
            // after that each following power of 2 will be the middle connector
            // at the next indent
            int start = 2;

            // The increment to start will always be 2^exp where exp increments
            // by one each time start is increased. exp is also used in the math
            // to determine the spacing and surrounding connectors.
            int exp = 2;

            while (start < lines.Count) {
                int index = start - 1;
                while (index < lines.Count) {
                    createVerticalConnectors(lines, charCount, index, exp);
                    index += (int) Math.Pow(2, exp);
                }

                exp++;
                start *= 2;
            }
        }

        /**
         * Creates the vertical connectors for the given line and surrounding
         * lines.
         * lines:       the list of lines
         * charCount:   the number of characters used for the name in a line
         * index:       the index of the line in lines
         * exp:         the exponent used to move index
         */
        private void createVerticalConnectors(List<string> lines, int charCount,
                int index, int exp) {
            // Create the vertical connector for this line
            createVerticalConnector(lines, charCount, index, exp);
            
            // Create the surrounding vertical connectors
            createSurroundingVerticalConnectors(lines, charCount, index, exp);
        }

        /**
         * Creates the vertical connectors for the surrounding lines.
         * Each line needs the surrounding 2 ^ (exp - 2) lines to have a
         * vertical connector with it. For example, on an eight person bracket,
         * line index 3 would also need the lines and indices 2 and 4 to have
         * a connector and line index 7 would need lines at indices [4, 10] to
         * have a connector
         * lines:       the list of lines
         * charCount:   the number of characters used for the name in a line
         * index:       the index of the line in lines
         * exp:         the exponent used to move index
         */
        private void createSurroundingVerticalConnectors(List<string> lines,
                int charCount, int index, int exp) {
            int offset = (int) Math.Pow(2, exp - 2);
            for (int i = 1; i < offset; i++) {
                createVerticalConnector(lines, charCount, index - i, exp);
                createVerticalConnector(lines, charCount, index + i, exp);
            }
        }

        /**
         * Adds the vertical connector to the given line.
         * lines:       the list of lines
         * charCount:   the number of characters used for the name in a line
         * index:       the index of the line in lines
         * exp:         the exponent used to move index
         */
        private void createVerticalConnector(List<string> lines, int charCount,
                int index, int exp) {
            // The index to place the "|" at on the line
            int strIndex = ((charCount + 1) * (exp - 1)) + (exp - 2);

            // Pad with extra spaces if needed
            int length = lines[index].Length;
            for (int i = 0; i < strIndex - length + 1; i++) {
                lines[index] += " ";
            }

            // Insert "|"
            char[] chars = lines[index].ToCharArray();
            chars[strIndex] = '|';
            lines[index] = new string(chars);
        }

        /**
         * Gets the length of the longest name in the bracket
         */
        private int longestName() {
            return longestName(root.top);
        }

        /**
         * Helper function to recursively get the longest name
         * node:    the Node to check
         */
        private int longestName(Node node) {
            if (node == null) {
                return 0;
            }

            int childrenLongest = Math.Max(longestName(node.top),
                    longestName(node.bottom));
            return Math.Max(node.name.Length, childrenLongest);
        }

        /**
         * Gets the number of layers the bracket will consist of
         */
        private int numLayers() {
            int largestBracket = this.numCompetitors + byes();
            return (int) (Math.Log(largestBracket) / Math.Log(2)) + 1;
        }

        /**
         * A node in the bracket.
         * Each node has a name associated with it and a top and bottom Node
         * that represent the two Nodes that lead to it.
         */
        private class Node {
            public string name;
            public Node top;
            public Node bottom;

            /**
             * Creates a new Node with an empty name
             */
            public Node() : this("") {}

            /**
             * Creates a new Node with the given name
             * name:    the name to use
             */
            public Node(string name) {
                this.name = name;
                this.top = null;
                this.bottom = null;
            }

            /**
             * Returns the name for this Node
             */
            public override string ToString() {
                return name;
            }
        }
    }

}
